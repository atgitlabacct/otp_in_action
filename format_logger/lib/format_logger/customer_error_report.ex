defmodule FormatLogger.CustomErrorReport do
  use GenEvent

  ############################################################
  ## ExternalApi
  def register_with_logger do
    Logger.add_backend(__MODULE__)
    #:error_logger.add_report_handler(__MODULE__)
  end
  
  ############################################################
  ## Callbacks

  def handle_event({:info, _gleader, {Logger, message, timestamp, metadata}}, state) do
    IO.puts("INFO - #{inspect message} - State - #{state}")
    {:ok, state}
  end

  def handle_event(event, state) do
    IO.puts "Received event #{inspect event}"
    {:ok, state}
  end

  def handle_call(_msg, state) do
    {:reply, :ok, state}
  end

  def handle_info(_msg, state) do
    {:ok, state}
  end

  def terminate(_reason, _state) do
    :ok
  end
  
  ############################################################
  ## Interal Api
end
