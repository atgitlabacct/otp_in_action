defmodule DiePlease do
  use GenServer

  @server __MODULE__
  @sleep_time (2 * 1000)

  ############################################################
  ## ExternalApi
  def start_link do
    GenServer.start_link(__MODULE__, [])
  end
  
  ############################################################
  ## Callbacks

  def init([]) do
    IO.puts("Inializing")
    {:ok, :foo, @sleep_time} 
  end

  def handle_call(_msg, _from, state) do
    {:reply, :ok, state}
  end

  def handle_cast(_msg, state) do
    {:noreply, state}
  end

  def handle_info(:timeout, state) do
    IO.puts("Handling info timeout")
    :ima_gonna_cause = :an_exit
    {:noreply, state}
  end

  def terminate(_reason, _state) do
    :ok
  end

  ############################################################
  ## Interal Api

end
