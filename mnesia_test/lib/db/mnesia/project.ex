use Amnesia

defdatabase Database do
  deftable User, [{:id, autoincrement}, :name], type: :ordered_set, index: [:name] do
    @type t :: %User{id: integer, name: String.t}

    def add_user(name) do
      %User{name: name} |> User.write
    end
  end

  deftable Project, [:title, :description] do 
    @type t :: %Project{title: String.t, description: String.t}

  end

  deftable Contributor, [:user_id, :project_title] do
    @type t :: %Contributor{user_id: integer, project_title: String.t}
  end

end
