defmodule Mix.Tasks.Mnesia.Database do

  defmodule Install do
    use Mix.Task
    use Database

    def run(_) do
      IO.write "Creating mnesia schema..."
      Amnesia.Schema.create
      IO.puts "DONE"

      IO.write "Creating database..."
      Amnesia.start
      # Database.create(disk: [node])
      Database.create()
      Database.wait
      IO.puts "DONE"

      IO.write "Stopping amnesia..."
      Amnesia.stop
      IO.puts "DONE"
    end
  end


  defmodule Uninstall do
    use Mix.Task
    use Database

    def run(_) do
      IO.write "Destroying database..."
      Amnesia.start

      Database.destroy

      Amnesia.stop
      Amnesia.Schema.destroy
      IO.puts "DONE"
    end
  end
end
