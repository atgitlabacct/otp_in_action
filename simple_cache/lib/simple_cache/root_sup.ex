defmodule SimpleCache.RootSup do
  use Supervisor

  @server __MODULE__

  def start_link do
    Supervisor.start_link(__MODULE__, [], name: @server)
  end

  def init([]) do
    children = [
      worker(SimpleCache.ElementSup, [], shutdown: 2000),
      worker(SimpleCache.Event, [], shutdown: 2000)]

    opts = [ strategy: :one_for_one, max_restarts: 4, max_seconds: 3600 ]
    supervise(children, opts)
  end
  
end
