defmodule SimpleCache.App do
  def start(_start_type, _start_args) do
    SimpleCache.Store.init()
    case SimpleCache.RootSup.start_link do
      {:ok, pid} -> 
        SimpleCache.EventLogger.add_handler
        {:ok, pid}
      other ->{:error, other}
    end
  end

  def stop do
    :ok
  end
end
