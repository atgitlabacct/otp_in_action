defmodule SimpleCache.Event do
  @server __MODULE__

  ############################################################
  ## ExternalApi
  
  def start_link do
    GenEvent.start_link(name: @server)
  end

  @doc """
  Add an event handler to the event manager
  """
  def add_handler(handler, args) do
    # GenEvent will call the init method on the handler and initialize it
    # to its valid state.
    GenEvent.add_handler(@server, handler, args)
  end

  def delete_handler(handler, args) do
    GenEvent.delete_handler(@server, handler, args)
  end

  def lookup(key), do: GenEvent.notify(@server, {:lookup, key})

  def fetch(key), do: GenEvent.notify(@server, {:fetch, key})

  def create(key, value), do: GenEvent.notify(@server, {:create, {key, value}})

  def replace(key, value), do: GenEvent.notify(@server, {:replace, {key, value}})

  def delete(key), do: GenEvent.notify(@server, {:delete, key})

  ############################################################
  ## Callbacks

  def init([]) do
    :ok
  end
  
  ############################################################
  ## Interal Api

end
