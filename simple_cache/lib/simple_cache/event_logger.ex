defmodule SimpleCache.EventLogger do
  require Logger

  ############################################################
  ## ExternalApi
  def add_handler, do: SimpleCache.Event.add_handler(__MODULE__, [])

  def delete_handler, do: SimpleCache.Event.delete_handler(__MODULE__, [])

  def handle_event({:create, {key, value}}, state) do
    Logger.info("create(#{inspect key}, #{inspect value})")
    {:ok, state}
  end

  def handle_event({:lookup, key}, state) do
    Logger.info("lookup(#{inspect key})")
    {:ok, state}
  end

  def handle_event({:fetch, key}, state) do
    Logger.info("fetch from strore (#{inspect key})")
    {:ok, state}
  end

  def handle_event({:delete, key}, state) do
    Logger.info("delete(#{inspect key})")
    {:ok, state}
  end
  
  def handle_event({:replace, {key, value}}, state) do
    Logger.info("replace(#{inspect key}, #{inspect value})")
    {:ok, state}
  end
  
  ############################################################
  ## Callbacks
  def init([]) do
    {:ok, []}
  end
  
  ############################################################
  ## Interal Api
end
