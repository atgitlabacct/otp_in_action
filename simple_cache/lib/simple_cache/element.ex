defmodule SimpleCache.Element do
  use GenServer

  @lease_time (60 * 60 * 24) # One day
  @server __MODULE__

  # require Record
  # Record.defrecord :cache, [:value, :start_time, lease_time: @lease_time]

  ############################################################
  ## ExternalApi
  def start_link(value, lease_time) do
    GenServer.start_link(__MODULE__, [value, lease_time])
  end

  def create(value, lease_time \\ @lease_time) do
    SimpleCache.ElementSup.start_child(value, lease_time)
  end

  def fetch(pid), do: GenServer.call(pid, :fetch)

  def replace(pid, value), do: GenServer.cast(pid, {:replace, value})

  def delete(pid), do: GenServer.cast(pid, :delete)

  def delete_sync(pid), do: GenServer.call(pid, :delete)

  ############################################################
  ## Callbacks

  def init([value, lease_time]) do
    start_time = current_time()
    {:ok, %{value: value, lease_time: lease_time, start_time: start_time}, time_left(start_time, lease_time)}
  end

  def handle_call(:fetch, _from, state) do
    # TODO: Read best way to access records and fix access
    value = state.value
    lease_time = state.lease_time
    start_time = state.start_time

    {:reply, {:ok, value}, state, time_left(start_time, lease_time)}
  end

  def handle_call(:delete, _from, state) do
    delete 
    {:stop, :normal, :ok, state}
  end

  def handle_cast({:replace, new_value}, state) do
    lease_time = state.lease_time
    start_time = state.start_time

    {:noreply, %{state | value: new_value}, time_left(start_time, lease_time)}
  end

  def handle_cast(:delete, state) do
    {:stop, :normal, state}
  end

  def handle_info(:timeout, state) do
    {:stop, :normal, state}
  end

  def terminate(_reason, _state) do
    delete 
    :ok
  end

  ############################################################
  ## Interal Api
  defp time_left(_start_time, :infinity), do: :infinity
  defp time_left(start_time, lease_time) do
    time_elapsed = current_time() - start_time
    case lease_time - time_elapsed do
      time when time <= 0 -> 0
      time                -> time * 1000
    end
  end

  defp delete, do: SimpleCache.Store.delete(self)
  

  defp current_time, do: :calendar.datetime_to_gregorian_seconds(:calendar.local_time())
end
