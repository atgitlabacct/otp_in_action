defmodule SimpleCache.ElementSup do
  use Supervisor

  @server __MODULE__

  ############################################################
  ## ExternalApi
  def start_link do
    Supervisor.start_link(__MODULE__, [], name: @server)
  end

  def start_child(value, lease_time) do
    Supervisor.start_child(__MODULE__, [value, lease_time])
  end
  
  ############################################################
  ## Callbacks
  def init([]) do
    # If a child does it should not be restarted (restart: temporary)
    children = [
      worker(SimpleCache.Element, [], shutdown: :brutal_kill, restart: :temporary)
    ]

    supervise(children, strategy: :simple_one_for_one, max_restarts: 0, max_seconds: 1)
  end
  
  ############################################################
  ## Interal Api
end
