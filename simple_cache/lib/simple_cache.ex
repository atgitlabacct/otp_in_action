defmodule SimpleCache do
  def insert(key, value) do
    case SimpleCache.Store.lookup(key) do
      {:ok, pid} -> 
        SimpleCache.Element.replace(pid, value)
        SimpleCache.Event.replace(key, value)
      {:error, _} ->
        {:ok, pid} = SimpleCache.Element.create(value)
        SimpleCache.Store.insert(key, pid)
        SimpleCache.Event.create(key, value)
    end
  end

  def lookup(key) do
    try do
      {:ok, pid} = SimpleCache.Store.lookup(key)
      SimpleCache.Event.lookup(key)
      {:ok, value} = SimpleCache.Element.fetch(pid)
      SimpleCache.Event.fetch(key)
      {:ok, value}
    rescue
      error -> {:error, error}
    end
  end

  def delete(key) do
    case SimpleCache.Store.lookup(key) do
      {:ok, pid} -> SimpleCache.Element.delete(pid)
      {:error, _reason} -> :ok
    end
  end

  def delete_sync(key) do
    case SimpleCache.Store.lookup(key) do
      {:ok, pid} -> SimpleCache.Element.delete_sync(pid)
      {:error, _reason} -> :ok
    end
  end

end
