defmodule SimpleCacheTest do
  use ExUnit.Case, async: false
  
  setup_all do
    SimpleCache.App.start(:ignored_arg_1, :ignored_arg_2)

    on_exit fn ->
      SimpleCache.App.stop
    end
  end

  test "inserting and looking up a new value" do
    SimpleCache.insert(:myval, 100)
    assert {:ok, 100} = SimpleCache.lookup(:myval)
  end

  test "insert over the top of exist value" do
    SimpleCache.insert(:myval, 500)
    assert {:ok, 500} = SimpleCache.lookup(:myval)
    SimpleCache.insert(:myval, 501)
    assert {:ok, 501} = SimpleCache.lookup(:myval)
  end

  test "deleting a key removes the value" do
    SimpleCache.insert(:myval, 200)

    assert :ok = SimpleCache.delete_sync(:myval)
    assert {:error, _}= SimpleCache.lookup(:myval)
  end
end
