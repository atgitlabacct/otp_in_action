defmodule SimpleCache.StoreTest do
  use ExUnit.Case

  test "can insert and lookup a key" do
    SimpleCache.Store.init()
    assert(true = SimpleCache.Store.insert(:foo, 100))
    assert({:ok, 100} = SimpleCache.Store.lookup(:foo))
  end

  test "delets a key" do
    SimpleCache.Store.init()

    SimpleCache.Store.insert(:foo, 100)
    SimpleCache.Store.delete(100)

    assert {:error, :not_found} = SimpleCache.Store.lookup(:foo) 
  end

end
