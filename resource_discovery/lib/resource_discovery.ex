defmodule ResourceDiscovery do
  use GenServer

  @server __MODULE__

  ############################################################
  ## ExternalApi
  def start_link, do: GenServer.start_link(__MODULE__, [], name: @server, debug: [ :trace ])
  
  def add_target_resource_type(type) do
    GenServer.cast(@server, {:add_target_resource_type, type})
  end

  def add_local_resource(type, instance) do
    GenServer.cast(@server, {:add_local_resource, {type, instance}})
  end

  def fetch_resources(type) do
    GenServer.call(@server, {:fetch_resources, type})
  end

  def fetch_local_resources do
    GenServer.call(@server, :fetch_local_resources)
  end

  def trade_resources(), do: GenServer.cast(@server, :trade_resources)

  ############################################################
  ## Callbacks

  def init([]) do
    state = %{target_resource_types: [], 
              local_resource_tuples: %{},
              found_resource_tuples: %{}}

    {:ok, state}
  end

  def handle_call({:fetch_resources, type}, _from, state) do
    resources = Map.fetch(state.found_resource_tuples, type)
    {:reply, resources, state}
  end

  def handle_call(:fetch_local_resources, _from, state) do
    {:reply, state.local_resource_tuples, state}
  end

  def handle_cast({:add_target_resource_type, type}, state) do
    target_types = state.target_resource_types
    new_target_types = [ type |  List.delete(target_types, type)]
    
    {:noreply, Map.put(state, :target_resource_types, new_target_types)}
  end

  def handle_cast({:add_local_resource, {type, instance}}, state) do
    resource_tuples = state.local_resource_tuples
    new_resource_tuples = add_resource(type, instance, resource_tuples)

    {:noreply, Map.put(state, :local_resource_tuples, new_resource_tuples)}
  end

  def handle_cast(:trade_resources, state) do
    resource_tuples = state.local_resource_tuples
    all_nodes = [node() | Node.list]
    Enum.each(all_nodes, fn(n) ->
      GenServer.cast({@server, n}, {:trade_resources, {node(), resource_tuples}})
    end)

    {:noreply, state}
  end

  def handle_cast({:trade_resources, {reply_to, remotes}}, state) do
    %{local_resource_tuples: locals,
      target_resource_types: target_types,
      found_resource_tuples: old_found} = state

    filtered_remotes = resources_for_types(target_types, remotes)
    new_found = add_resources(filtered_remotes, old_found)
    case reply_to do
      :noreply ->
        :ok
      _ ->
        GenServer.cast({@server, reply_to}, {:trade_resources, {:noreply, locals}})
    end
    
    {:noreply, %{state | found_resource_tuples: new_found}}
  end

  ############################################################
  ## Interal Api
  defp add_resource(type, resource, resource_tuples) do
    case Map.fetch(resource_tuples, type) do
      {:ok, resource_list} -> 
        IO.puts "Resource list #{inspect resource_list}"
        new_list = [resource | List.delete(resource_list, resource)]
        Map.put(resource_tuples, type, new_list)
      :error ->
        Map.put(resource_tuples, type, [resource])
    end
  end

  defp add_resources([{type, resource} | tail], resource_tuples) do
    add_resources(tail, add_resource(type, resource, resource_tuples))
  end

  defp add_resources([], resource_tuples), do: resource_tuples

  defp resources_for_types(types, resource_tuples) do
    fun = fn type, acc -> 
      case Dict.fetch(resource_tuples, type) do
        {:ok, list} ->
          for instance <- list do
            {type, instance}
          end ++ acc
        :error ->
          acc
      end
    end

    List.foldl(types, [], fun)
  end
end
