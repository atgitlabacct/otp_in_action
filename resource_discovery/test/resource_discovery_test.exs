defmodule ResourceDiscoveryTest do
  use ExUnit.Case

  test "adding a local resources saves it in the state" do
    type = :adding_fns
    {:ok, pid} = ResourceDiscovery.start_link
    add_one = fn(n) -> n + 1 end
    add_two = fn(n) -> n + 1 end
    ResourceDiscovery.add_local_resource(type, add_one)
    ResourceDiscovery.add_local_resource(type, add_two)

    resource_map = ResourceDiscovery.fetch_local_resources
    assert Enum.member?(resource_map.adding_fns, add_one)
    assert Enum.member?(resource_map.adding_fns, add_two)
  end

  test "finds target resources locally" do
    type = :adding_fns
    {:ok, pid} = ResourceDiscovery.start_link
    add_one = fn(n) -> n + 1 end
    ResourceDiscovery.add_local_resource(type, add_one)
    ResourceDiscovery.add_target_resource_type(type)
    :ok = ResourceDiscovery.trade_resources

    :timer.sleep(3000)

    {:ok, resources} = ResourceDiscovery.fetch_resources(type)
    assert(Enum.member?(resources, add_one))
  end

end
