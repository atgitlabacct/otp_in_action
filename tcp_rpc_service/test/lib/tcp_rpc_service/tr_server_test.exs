defmodule TrServerTest do
  use ExUnit.Case

  test "it starts the server with the default name" do
    {:ok, pid} = TrServer.start_link()
    assert pid == Process.whereis(:"Elixir.TrServer") 
    
    {:ok, socket} = :gen_tcp.connect({127,0,0,1}, 1055, [])
    :gen_tcp.send(socket, "erlang:abs(1.1).")
    :gen_tcp.close(socket);

    TrServer.stop()
  end

  test "keeps a record of the count of tcp messages" do
    {:ok, pid} = TrServer.start_link()

    {:ok, socket} = :gen_tcp.connect({127,0,0,1}, 1055, [])
    :gen_tcp.send(socket, "erlang:abs(1.1).")
    :gen_tcp.send(socket, "erlang:abs(1.1).")
    :gen_tcp.close(socket);
    assert 2 == TrSevrver.get_count

    TrServer.stop()
  end
end
