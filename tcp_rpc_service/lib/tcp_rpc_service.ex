defmodule TcpRpcService do
  use Application

  def start(_type, _args) do
    IO.puts "Starting application"
    case TrSupervisor.start_link do
      {:ok, pid} ->
        {:ok, pid}
      other ->
        {:error, other}
    end
  end

  def stop(_state) do
    IO.puts "Stopped"
    :ok
  end
end
