defmodule TrServer do
  @doc """
  RPC over TCP server.  This module defines a server process that
  listens for incoing TCP connections and allows the user to
  exeute RPC commands via that TCP stream.
  """
  defmodule State do
    defstruct port: 0, lsock: "", request_count: 0
  end

  use GenServer

  @server __MODULE__
  @default_port 1055

  ##################
  ## External API
  @doc """
  Starts the server
  """
  @spec start_link(port::Integer) :: GenServer.on_start
  def start_link(port, opts \\ []) do
    GenServer.start_link(__MODULE__, [ port ], [{:name, @server} | opts])
  end

  @doc """
  Call start_link(port) with the default port
  """
  @spec start_link() :: GenServer.on_start
  def start_link, do: start_link(@default_port)

  @doc """
  Fetches the number of requests made to this server.
  """
  @spec get_count :: {:ok, count::Integer}
  def get_count, do: GenServer.call(@server, :get_count)

  @doc """
  Stop the server
  """
  @spec stop :: :ok
  def stop, do: GenServer.cast(@server, :stop)

  ##################
  ## OTP Callbacks
  def init([ port ]) do
    IO.puts "Initializing"
    {:ok, lsock} = :gen_tcp.listen(port, [{:active, true}])
    {:ok, %TrServer.State{port: port, lsock: lsock}, 0}
  end

  def handle_call(:get_count, _from, state) do
    {:reply, state.request_count, state}
  end

  def handle_cast(:stop, state) do
    {:stop, :normal, state}
  end

  @doc """
  The generic format for the basic text protocol in raw_data param is
  Module:function(Arg1, ..., ArgN)
  """
  def handle_info({:tcp, socket, raw_data}, state) do
    IO.puts "Received tcp connection"
    do_rpc(socket, raw_data)
    {:noreply, Map.update!(state, :request_count, &(&1 + 1))}
  end

  def handle_info(:timeout, state) do
    IO.puts "Listening"
    {:ok, _sock} = :gen_tcp.accept(state.lsock)
    {:noreply, state}
  end

  def code_change(_old_version, state, _extra) do
    {:ok, state}
  end

  ##################
  ## Internal API
  defp do_rpc(socket, raw_data) do
    try do
      {mod, func, args} = split_out_mfa(raw_data)
      result = apply(mod, func, args)
      :gen_tcp.send(socket, :io_lib.fwrite("~p~n", [result]))
    rescue
       err -> :gen_tcp.send(socket, :io_lib.fwrite("~p~n", [err]))
    end
  end

  defp split_out_mfa(raw_data) do
    mfa = :re.replace(raw_data, "\r\n$", "", [{:return, :list}])
    {:match, [m, f, a]} = :re.run(mfa, 
                                 "(.*):(.*)\s*\\((.*)\s*\\)\s*.\s*$",
                                 [{:capture, [1,2,3], :list}, :ungreedy])

    {List.to_atom(m), List.to_atom(f), args_to_terms(a)}
  end

  defp args_to_terms(raw_args) do
    {:ok, toks, _line} = :erl_scan.string('[' ++ raw_args ++ ']. ', 1)
    {:ok, args} = :erl_parse.parse_term(toks)
    args
  end
end
