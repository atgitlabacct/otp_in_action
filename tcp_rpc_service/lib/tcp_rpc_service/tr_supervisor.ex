defmodule TrSupervisor do
  use Supervisor

  @server __MODULE__

  ############################################################
  ## ExternalApi

  def start_link do
    Supervisor.start_link(@server, [], name: @server)
  end

  ############################################################
  ## Callbacks

  def init([]) do
    # The original erlang code sets the children with a tuple of
    # {tr_server, {tr_server, start_link, []},
    #    permanent, 2000, worker, [tr_server]}
    # Most of the above is provided by the defaults in elixir except for the
    # shutdown value, which we provide
    children = [
      worker(TrServer, [], shutdown: 2000)
    ]

    # Erlang original code was tuple of RestartStrategy = {one_for_one, 0, 1}
    # Max restarts of 0 within 1 seconds means we allow no automatic restarts
    # this makes it easy to see problems that occur
    supervise(children, strategy: :one_for_one, max_restarts: 0, max_seconds: 1)
  end

  ############################################################
  ## Interal Api
end
